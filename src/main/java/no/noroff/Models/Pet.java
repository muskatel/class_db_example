package no.noroff.Models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;

@Entity
public class Pet {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String name;

    @Column
    public Integer age;


    @JsonGetter("owner")
    public String getOwner()
    {
        if(owner != null)
        {
            return "/person/" + owner.id;
        }
        else
        {
            return null;
        }
    }

    @ManyToOne()
    @JoinTable(
            name = "owner",
            joinColumns = {@JoinColumn(name = "pet_id")},
            inverseJoinColumns = {@JoinColumn(name = "owner_id")}
    )
    public Person owner;
}
