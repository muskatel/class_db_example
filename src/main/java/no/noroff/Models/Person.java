package no.noroff.Models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Person {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String name;

    @Column
    public String contact;


    @JsonGetter("pets")
    public List<String> get_pets_list(){
        return pets.stream()
                .map(pet -> {
                    return "/pet/" +pet.id;
                }).collect(Collectors.toList());
    }

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    List<Pet> pets = new ArrayList<>();
}
