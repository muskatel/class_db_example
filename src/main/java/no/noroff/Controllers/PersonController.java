package no.noroff.Controllers;

import no.noroff.Models.Person;
import no.noroff.Repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    //create
    @PostMapping("/person")
    public Person createPerson(@RequestBody Person person)
    {
        person = personRepository.save(person);
        return person;
    }

    //Read

    @GetMapping("/person/{id}")
    public Person getPerson(@PathVariable Integer id)
    {
        Person person = null;
        if(personRepository.existsById(id))
        {
            // find will guaranteed find a unique pet
            person = personRepository.findById(id).get();
        }
        return person;
    }
}
