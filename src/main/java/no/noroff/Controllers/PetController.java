package no.noroff.Controllers;

import no.noroff.Models.Pet;
import no.noroff.Repositories.PetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PetController {

    @Autowired
    private PetRepository petRepository;


    // Create

    @PostMapping("/pet")
    public Pet createPet(@RequestBody Pet pet)
    {
        pet = petRepository.save(pet);
        return pet;
    }

    //Read

    @GetMapping("/pet/{id}")
    public Pet getPet(@PathVariable Integer id)
    {
        Pet pet = null;
        if(petRepository.existsById(id))
        {
            // find will guaranteed find a unique pet
            pet = petRepository.findById(id).get();
        }
        return pet;
    }

    @GetMapping("/pet/all")
    public List<Pet> getAllPets()
    {
        return petRepository.findAll();
    }
}
