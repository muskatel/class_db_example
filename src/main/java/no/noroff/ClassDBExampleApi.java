package no.noroff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassDBExampleApi {

    public static void main(String[] args) {
        SpringApplication.run(ClassDBExampleApi.class, args);
    }
}
